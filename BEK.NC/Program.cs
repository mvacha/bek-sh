﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using BEK.Shared;
using ProcessPrivileges;

namespace BEK.NC
{
    class Program
    { 
        static void Main(string[] args)
        {
            DEPHelper.ForceDEP();
            PrivilegesHelper.KeepAllExcept(new[] { Privilege.SystemTime });

            long ticks = 0;
            if (args.Length != 1 || !long.TryParse(args[0], out ticks))
                Environment.Exit(-1);

            var newDate = DateTime.UtcNow.Date + TimeSpan.FromTicks(ticks);
            var sysDate = new SYSTEMTIME(newDate);

            if (SetSystemTime(ref sysDate))
                Environment.Exit(0);
            else
                Environment.Exit(-2);
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool SetSystemTime(ref SYSTEMTIME time);

        [StructLayout(LayoutKind.Sequential)]
        public struct SYSTEMTIME
        {
            public ushort Year;
            public ushort Month;
            public ushort DayOfWeek;
            public ushort Day;
            public ushort Hour;
            public ushort Minute;
            public ushort Second;
            public ushort Milliseconds;

            public SYSTEMTIME(DateTime dt)
            {
                Year = (ushort)dt.Year;
                Month = (ushort)dt.Month;
                DayOfWeek = (ushort)dt.DayOfWeek;
                Day = (ushort)dt.Day;
                Hour = (ushort)dt.Hour;
                Minute = (ushort)dt.Minute;
                Second = (ushort)dt.Second;
                Milliseconds = (ushort)dt.Millisecond;
            }
        }
    }
}
