﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace BEK.SH
{
    public static class StrongNameHelper
    {
        [DllImport("mscoree.dll", CharSet = CharSet.Unicode)]
        static extern bool StrongNameSignatureVerificationEx(string wszFilePath, bool fForceVerification, ref bool pfWasVerified);

        public static bool VerifyStrongName(string path)
        {
            bool notForced = false;
            bool verified = StrongNameSignatureVerificationEx(path, false, ref notForced);

            return verified && notForced;
        }

        public static bool CheckPublicKey(string path, string pubKeyPath)
        {
            if (!VerifyStrongName(path) || !File.Exists(pubKeyPath))
                return false;

            try
            {
                var asmKey = Assembly.LoadFrom(path).GetName().GetPublicKey();
                var key = LoadPublicKey(pubKeyPath);

                if (asmKey.Length != key.Length)
                    return false;

                for (int i = 0; i < asmKey.Length; i++)
                    if (asmKey[i] != key[i])
                        return false;

                return true;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
            catch (BadImageFormatException)
            {
                // the given file couldn’t get through the loader
                return false;
            }
        }

        private static byte[] LoadPublicKey(string pubKeyPath)
        {
            using (var stream = File.Open(pubKeyPath, FileMode.Open))
            {
                var buffer = new byte[160];
                var read = stream.Read(buffer, 0, buffer.Length);

                if (read != 160 || stream.Length != 160)
                    throw new Exception("Invalid public key file");

                return buffer;
            }
        }
    }
}
