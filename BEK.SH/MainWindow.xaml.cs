﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using BEK.Shared;
using static BEK.SH.DateHelper;
using ProcessPrivileges;
using System.ComponentModel;

namespace BEK.SH
{
    public partial class MainWindow : Window
    {
        private string _timeFormat = @"hh\:mm\:ss";

        private readonly DispatcherTimer _timer;

        public MainWindow()
        {
            DEPHelper.ForceDEP();
            PrivilegesHelper.RemoveAll();

            InitializeComponent();

            _timer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(200) };
            _timer.Tick += (s, e) => TimeText.Text = GetTime(_timeFormat);

            Loaded += (s, e) => _timer.Start();
            TimeSpanControl.Loaded += TimeSpanControl_Loaded;
        }

        private void ChangeFormat_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(FormatTextBox.Text) || !ValidateFormat(FormatTextBox.Text))
            {
                MessageBox.Show("Neplatný formát času!");
            }
            else
            {
                _timeFormat = FormatTextBox.Text;
            }
        }

        private void ChangeTime_Click(object sender, RoutedEventArgs e)
        {
            if (!TimeSpanControl.Value.HasValue)
            {
                MessageBox.Show("Vyplňte čas!");
                return;
            }

            //entered time is in local time, but NC requires in UTC time
            var time = DateTime.Now.Date + TimeSpanControl.Value.Value;
            var ticks = time.ToUniversalTime().TimeOfDay.Ticks;

            var appDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var ncPath = Path.Combine(appDir, "BEK.NC.exe");
            var keyPath = Path.Combine(appDir, "PubKey.snk");

            if (!StrongNameHelper.CheckPublicKey(ncPath, keyPath))
                return;

            var ncInfo = new ProcessStartInfo(ncPath, ticks.ToString());
            try
            {
                Process.Start(ncInfo);
            }
            catch(Win32Exception ex)
            {
                MessageBox.Show("Pro změnu hodin je potřeba administrátorských práv!");
            }
        }

        private void TimeSpanControl_Loaded(object sender, RoutedEventArgs e)
        {
            //hide day part
            foreach (var elemName in new[] { "NumericTBDays", "Separator1", "TextBlockD" })
            {
                var el = TimeSpanControl.FindName(elemName) as UIElement;
                el?.SetCurrentValue(StyleProperty, null);
                el?.SetCurrentValue(VisibilityProperty, Visibility.Collapsed);
            }

            //disable edit mode
            foreach (var elemName in new[] { "TextBlockH", "TextBlockM", "TextBlockS" })
            {
                var el = TimeSpanControl.FindName(elemName) as UIElement;
                el?.SetCurrentValue(IsHitTestVisibleProperty, false);
            }

            //force correct border style without breaking the nested border elements
            var border = TimeSpanControl.FindName("MainContainer") as UIElement;
            border.SetCurrentValue(StyleProperty, Resources["TimeSpanBorderStyle"]);

            TimeSpanControl.Value = DateTime.Now.TimeOfDay;
        }
    }
}
