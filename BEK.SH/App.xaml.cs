﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows;

namespace BEK.SH
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private TcpListener _server;

        private async void Application_Startup(object sender, StartupEventArgs e)
        {
            var port = GetPortFromReg(1050);
            if (port < 1024 || port > IPEndPoint.MaxPort)
            {
                MessageBox.Show($"Port je mimo rozsah: <1024, {IPEndPoint.MaxPort}>");
                return;
            }

            _server = new TcpListener(IPAddress.Any, port);

            try
            {
                _server.Start();

                while (true)
                {
                    var client = await _server.AcceptTcpClientAsync().ConfigureAwait(false);
                    HandleClient(client);
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show($"Chyba TCP serveru kód chyby: {ex.ErrorCode}");
            }
        }

        private int GetPortFromReg(int defaultPort)
        {
            using (var swReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\SitoveHodiny"))
            {
                if (swReg == null)
                {
                    using (var newReg = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\SitoveHodiny"))
                        newReg.SetValue("Port", defaultPort, RegistryValueKind.DWord);

                    return defaultPort;
                }

                if(swReg.GetValue("Port") is int regPort)
                {
                    return regPort;
                }
                else
                {
                    return defaultPort;
                }
            }
        }

        private async void HandleClient(TcpClient client)
        {
            try
            {
                using (var stream = client.GetStream())
                using (var reader = new StreamReader(stream))
                using (var writer = new StreamWriter(stream))
                {
                    var line = await reader.ReadLineAsync().ConfigureAwait(false);
                    if (line == default(string)) return;

                    if (string.IsNullOrEmpty(line) || DateHelper.ValidateFormat(line))
                    {
                        await writer.WriteLineAsync(DateHelper.GetTime(line)).ConfigureAwait(false);
                    }
                    else
                    {
                        await writer.WriteLineAsync("Neplatny format!").ConfigureAwait(false);
                    }
                    await writer.FlushAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
               //something failed for one client
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e) => _server.Stop();
    }
}
