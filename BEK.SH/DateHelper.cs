﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BEK.SH
{
    public static class DateHelper
    {
        public static string GetTime(string format) => DateTime.Now.TimeOfDay.ToString(format);
        public static bool ValidateFormat(string format)
        {
            try
            {
                GetTime(format);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
