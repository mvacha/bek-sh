﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BEK.Shared
{
    public static class DEPHelper
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool SetProcessDEPPolicy(uint dwFlags);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool GetProcessDEPPolicy(IntPtr hProcess, out uint lpFlags, out bool lpPermanent);

        public static bool TurnOnDEP()
        {
            var handle = Process.GetCurrentProcess().Handle;
            var res = GetProcessDEPPolicy(handle, out var flags, out var permanent);
            if (res && flags == 3) //ENABLED and DISABLE_ATL_THUNK_EMULATION
                return true;

            var dep = SetProcessDEPPolicy(3);
            var err = Marshal.GetLastWin32Error();

            if (dep || err == 50) //Error 50 means we are running on x64 and DEP is enforced
                return true;
            else
                return false;
        }

        public static void ForceDEP()
        {
            if (!TurnOnDEP())
                Environment.Exit(-1);
        }
    }
}
