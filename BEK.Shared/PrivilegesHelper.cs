﻿using ProcessPrivileges;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BEK.Shared
{
    public static class PrivilegesHelper
    {
        public static void PrintAll()
        {
            var process = Process.GetCurrentProcess();
            var privileges = process.GetPrivileges();

            Debug.WriteLine($"Privileges for process {process.ProcessName}:");

            foreach (var privilege in privileges)
            {
                Debug.WriteLine($"{privilege.Privilege} - {privilege.PrivilegeState}");
            }
        }

        public static void RemoveAll() => KeepAllExcept(Enumerable.Empty<Privilege>());

        public static void KeepAllExcept(IEnumerable<Privilege> excluded)
        {
            var process = Process.GetCurrentProcess();
            var privileges = process.GetPrivileges();

            Debug.WriteLine($"Privileges for process {process.ProcessName}:");

            foreach (var privilege in privileges.Select(p => p.Privilege).Except(excluded))
            {
                var result = process.RemovePrivilege(privilege);
            }
        }
    }
}
